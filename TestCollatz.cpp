// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <sstream>  // istringtstream, ostringstream

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read_1) {
    istringstream r("1 10\n");
    int i;
    int j;
    istream& s = collatz_read(r, i, j);
    ASSERT_TRUE(s);
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
}

TEST(CollatzFixture, read_2) {
    istringstream r("10 100\n");
    int i;
    int j;
    istream& s = collatz_read(r, i, j);
    ASSERT_TRUE(s);
    ASSERT_EQ(i,  10);
    ASSERT_EQ(j, 100);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval_1) {
    const int v = collatz_eval(1, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_2) {
    const int v = collatz_eval(100, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval_3) {
    const int v = collatz_eval(201, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval_4) {
    const int v = collatz_eval(900, 1000);
    ASSERT_EQ(v, 174);
}

TEST(CollatzFixture, eval_5) {
    const int v = collatz_eval(100, 25);
    ASSERT_EQ(v, 119);
}

TEST(CollatzFixture, eval_6) {
    const int v = collatz_eval(75, 10);
    ASSERT_EQ(v, 116);
}

TEST(CollatzFixture, eval_7) {
    const int v = collatz_eval(684, 15);
    ASSERT_EQ(v, 145);
}

TEST(CollatzFixture, eval_8) {
    const int v = collatz_eval(500, 2500);
    ASSERT_EQ(v, 209);
}

TEST(CollatzFixture, eval_9) {
    const int v = collatz_eval(1578, 9698);
    ASSERT_EQ(v, 262);
}

TEST(CollatzFixture, eval_10) {
    const int v = collatz_eval(4895, 4621);
    ASSERT_EQ(v, 197);
}

TEST(CollatzFixture, eval_11) {
    const int v = collatz_eval(546, 467);
    ASSERT_EQ(v, 142);
}

TEST(CollatzFixture, eval_12) {
    const int v = collatz_eval(123, 48);
    ASSERT_EQ(v, 119);
}

TEST(CollatzFixture, eval_13) {
    const int v = collatz_eval(760112, 878869);
    ASSERT_EQ(v, 525);
}

TEST(CollatzFixture, eval_14) {
    const int v = collatz_eval(671269, 694984);
    ASSERT_EQ(v, 442);
}

TEST(CollatzFixture, eval_15) {
    const int v = collatz_eval(123, 187);
    ASSERT_EQ(v, 125);
}

// -----
// print
// -----

TEST(CollatzFixture, print_1) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

TEST(CollatzFixture, print_2) {
    ostringstream w;
    collatz_print(w, 1, 150, 200);
    ASSERT_EQ(w.str(), "1 150 200\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve_1) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", w.str());
}

TEST(CollatzFixture, solve_2) {
    istringstream r("61 48\n132 4789\n201 210\n519 496\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("61 48 113\n132 4789 238\n201 210 89\n519 496 124\n", w.str());
}
