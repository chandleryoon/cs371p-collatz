// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include "Collatz.hpp"

using namespace std;

long cache[1000000];

// ------------
// collatz_read
// ------------

istream& collatz_read (istream& r, int& i, int& j) {
    return r >> i >> j;
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    assert(i < 1000000 && j < 1000000);
    //Switch if reverse, had to set temp as long becuase would get big
    long temp = i;
    if (j < i) {
        i = j;
        j = temp;
    }
    int count;
    int max = 0;
    for (; i < j + 1; ++i) {
        count = 0;
        temp = i;
        while (temp > 1) {
            //gotta keep it in bounds
            if(temp < 1000000) {
                if(cache[temp] != 0) {
                    count += cache[temp];
                    break;
                }
            }
            if (temp % 2 == 1) {
                ++count;
                temp = (temp * 3) + 1;
            }//simple step through
            ++count;
            temp /= 2;
        }
        //lazy cache, adds all values it gets
        cache[i] = count;
        if (count > max)
            max = count;
    }
    return max + 1;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    int i;
    int j;
    while (collatz_read(r, i, j)) {
        const int v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}