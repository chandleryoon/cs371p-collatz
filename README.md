# CS371p: Object-Oriented Programming Collatz Repo

* Name: Chandler Yoon

* EID: cy5673

* GitLab ID: chandleryoon

* HackerRank ID: chandlerjyoon@gmail.com

* Git SHA: 5fe2e93a28f12233f990d1920878d02a8c1e6279

* GitLab Pipelines: https://gitlab.com/chandleryoon/cs371p-collatz/pipelines

* Estimated completion time: 5 hours

* Actual completion time: Around 6 hours

* Comments: None
